create or alter function sf_hash(@cod_venda varchar(10),
								 @cod_loja varchar(10),  
                                 @cod_produto varchar(10),            
                                 @cod_tipo_pagamento varchar(10),
                                 @data_venda varchar(10), 
                                 @volume varchar(10),
                                 @valor varchar(10))
returns varbinary(20)
as
begin
    return HASHBYTES('SHA1', @cod_venda + @cod_loja + @cod_produto + @cod_tipo_pagamento + @data_venda + @volume + @valor)
end;