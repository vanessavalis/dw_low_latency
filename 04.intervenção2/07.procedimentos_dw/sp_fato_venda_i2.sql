create or alter procedure sp_fato_venda_i2(@data_carga datetime)
as
begin
	set nocount on
	declare @id_venda bigint,
			@id_tempo bigint,
			@data_venda datetime,
			@id_loja int,
			@cod_loja int,
			@id_produto int,
			@cod_produto int,
			@id_tipo_pagamento int,
			@cod_tipo_pagamento int,
			@cod_venda bigint,
			@volume numeric(10,2),
			@valor numeric(10,2),
			@quantidade int = 1,
			@hash_code_fato varbinary(20),
			@hash_code_aux varbinary(20)

	declare c_venda cursor for
	select data_venda, cod_loja, cod_produto, cod_tipo_pagamento, cod_venda, volume, valor, hash_code
	from tb_aux_venda
	where data_carga = @data_carga

	open c_venda
	fetch c_venda into @data_venda,
					   @cod_loja,
					   @cod_produto,
					   @cod_tipo_pagamento,
					   @cod_venda,
					   @volume,
					   @valor,
					   @hash_code_aux

	while (@@fetch_status = 0)
	begin
		set @id_tempo = (select id_tempo from dim_tempo where data = @data_venda)
		set @id_loja = (select id_loja from dim_loja where cod_loja = @cod_loja)
		set @id_produto = (select id_produto from dim_produto where cod_produto = @cod_produto)
		set @id_tipo_pagamento = (select id_tipo_pagamento from dim_tipo_pagamento where cod_tipo_pagamento = @cod_tipo_pagamento)
		set @hash_code_fato = (select hash_code from fato_venda where cod_venda = @cod_venda)

		if @hash_code_fato is not null
		begin
			if @hash_code_fato != @hash_code_aux
			update fato_venda
			set id_tempo = @id_tempo,
				id_loja = @id_loja,
				id_produto = @id_produto,
				id_tipo_pagamento = @id_tipo_pagamento,
				cod_venda = @cod_venda,
				volume = @volume,
				valor = @valor,
				hash_code = @hash_code_aux
			where cod_venda = @cod_venda
			end
		else
			insert into fato_venda(id_tempo,
								   id_loja,
								   id_produto,
								   id_tipo_pagamento,
								   cod_venda,
								   volume,
								   valor,
								   hash_code)
			values(@id_tempo,
				   @id_loja,
				   @id_produto,
				   @id_tipo_pagamento,
				   @cod_venda,
				   @volume,
				   @valor,
				   @hash_code_aux)
	fetch c_venda into @data_venda,
					   @cod_loja,
					   @cod_produto,
					   @cod_tipo_pagamento,
					   @cod_venda,
					   @volume,
					   @valor,
					   @hash_code_aux
	end
	close c_venda
	deallocate c_venda
end



-- Teste

exec sp_fato_venda_i2 '20230423'

select * from fato_venda