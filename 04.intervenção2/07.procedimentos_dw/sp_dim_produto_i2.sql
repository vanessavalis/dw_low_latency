create or alter procedure sp_dim_produto_i2(@data_carga datetime)
as
begin
   set nocount on
   declare @cod_produto int,
           @produto varchar(100),
		   @cod_categoria int,
		   @categoria varchar(100)

	declare c_produtos cursor for
	select cod_produto, produto, cod_categoria,
	       categoria 
    from tb_aux_produto
	where data_carga = @data_carga

	open c_produtos
	fetch c_produtos into @cod_produto,
	                      @produto,
						  @cod_categoria,
						  @categoria
	 
	 while (@@fetch_status =0)
	 begin
	     if exists (select 1 from dim_produto
		            where cod_produto = @cod_produto)
	     begin
		     update dim_produto
			 set produto = @produto,
			     cod_categoria = @cod_categoria,
				 categoria = @categoria
             where cod_produto = @cod_produto
		 end
		 else
		     insert into dim_produto(cod_produto,
			                         produto,
									 cod_categoria,
									 categoria)
			 values(@cod_produto, @produto,
			        @cod_categoria, @categoria)
	     fetch c_produtos into @cod_produto,
	                           @produto,
						       @cod_categoria,
						       @categoria
	 end
	 close c_produtos
	 deallocate c_produtos
end


-- Teste

exec sp_dim_produto_i2 '20230423'

select * from dim_produto