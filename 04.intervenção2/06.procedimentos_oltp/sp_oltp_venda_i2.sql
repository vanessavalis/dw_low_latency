create or alter procedure sp_oltp_venda_i2(@data_carga datetime, @data_inicial datetime, @data_final datetime)
as
begin
	delete from tb_aux_venda
	where data_carga = @data_carga
	insert into tb_aux_venda(data_carga,
							 data_venda,
							 cod_loja,
							 cod_produto,
							 cod_tipo_pagamento,
							 cod_venda,
							 volume,
							 valor,
							 hash_code)
	select @data_carga,
		   data_venda,
		   cod_loja,
		   cod_produto,
		   cod_tipo_pagamento,
		   cod_venda,
		   volume,
		   valor,
		   dbo.sf_hash(str(COD_VENDA),
                       str(COD_LOJA),
                       str(COD_PRODUTO),
                       str(COD_TIPO_PAGAMENTO),  
                       convert(varchar(10), data_venda, 103),
                       str(volume,10,2), str(valor, 10,2))

	from tb_venda
	where data_venda >= @data_inicial and
	      data_venda <= @data_final
end


-- Teste

exec sp_oltp_venda_i2 '20230423', '20230101', '20230701'
exec sp_oltp_venda_i2 '20230423', '20230101', '20230201'
exec sp_oltp_venda_i2 '20230423', '20230101', '20230301'
exec sp_oltp_venda_i2 '20230423', '20230101', '20230401'
exec sp_oltp_venda_i2 '20230423', '20230101', '20230501'
exec sp_oltp_venda_i2 '20230423', '20230101', '20230601'
exec sp_oltp_venda_i2 '20230423', '20230101', '20230701'

select * from tb_aux_venda