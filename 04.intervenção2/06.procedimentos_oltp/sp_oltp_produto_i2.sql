create or alter procedure sp_oltp_produto_i2(@data_carga datetime)
as
begin
    delete from tb_aux_produto
	where data_carga = @data_carga
    insert into tb_aux_produto(data_carga,
	                           cod_produto,
							   produto,
							   cod_categoria,
							   categoria)
	select @data_carga, 
	       p.cod_produto,
		   p.produto,
		   c.cod_categoria,
		   c.categoria
    from tb_produto p inner join
	tb_categoria c
	on (p.cod_categoria = c.cod_categoria)
end


-- Teste

exec sp_oltp_produto_i2 '20230423'

select * from tb_aux_produto