create or alter procedure sp_dim_loja(@data_carga datetime)
as
begin
	set nocount on
	declare @cod_loja int,
			@loja varchar(100),
			@cidade varchar(100),
			@estado varchar(100),
			@sigla_estado varchar(2)

	declare c_loja cursor for
	select cod_loja, loja, cidade, estado, sigla_estado
	from tb_aux_loja
	where data_carga = @data_carga

	open c_loja
	fetch c_loja into @cod_loja,
					  @loja,
					  @cidade,
					  @estado,
					  @sigla_estado

	while (@@fetch_status = 0)
	begin
		if exists (select 1 from dim_loja
				   where cod_loja = @cod_loja)
		begin
			update dim_loja
			set loja = @loja,
				cidade = @cidade,
				estado = @estado,
				sigla_estado = @sigla_estado
			where cod_loja = @cod_loja
			end
			else
				insert into dim_loja(cod_loja,
									 loja,
									 cidade,
									 estado,
									 sigla_estado)
				values(@cod_loja, @loja, @cidade, @estado, @sigla_estado)
			fetch c_loja into @cod_loja,
							  @loja,
							  @cidade,
							  @estado,
							  @sigla_estado
	end
	close c_loja
	deallocate c_loja
end



-- Teste

exec sp_dim_loja '20230321'

select * from dim_loja