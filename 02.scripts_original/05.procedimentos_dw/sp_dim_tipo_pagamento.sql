create or alter procedure sp_dim_tipo_pagamento(@data_carga datetime)
as
begin
	set nocount on
	declare @cod_tipo_pagamento int,
			@tipo_pagamento varchar(100)

	declare c_pagamento cursor for
	select cod_tipo_pagamento, tipo_pagamento 
	from tb_aux_tipo_pagamento
	where data_carga = @data_carga

	open c_pagamento
	fetch c_pagamento into @cod_tipo_pagamento,
						   @tipo_pagamento

	while (@@fetch_status = 0)
	begin
		if exists (select 1 from dim_tipo_pagamento
				   where cod_tipo_pagamento = @cod_tipo_pagamento)
		begin 
			update dim_tipo_pagamento
			set tipo_pagamento = @tipo_pagamento
			where cod_tipo_pagamento = @cod_tipo_pagamento
		end
		else
			insert into dim_tipo_pagamento(cod_tipo_pagamento,
										   tipo_pagamento)
			values (@cod_tipo_pagamento, @tipo_pagamento)
		fetch c_pagamento into @cod_tipo_pagamento, 
							   @tipo_pagamento
	end
	close c_pagamento
	deallocate c_pagamento
end



-- Teste

exec sp_dim_tipo_pagamento '20230321'

select * from dim_tipo_pagamento