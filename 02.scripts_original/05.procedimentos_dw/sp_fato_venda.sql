create or alter procedure sp_fato_venda(@data_carga datetime)
as
begin
	set nocount on
	declare @id_venda bigint,
			@id_tempo bigint,
			@data_venda datetime,
			@id_loja int,
			@cod_loja int,
			@id_produto int,
			@cod_produto int,
			@id_tipo_pagamento int,
			@cod_tipo_pagamento int,
			@cod_venda bigint,
			@volume numeric(10,2),
			@valor numeric(10,2),
			@quantidade int = 1

	declare c_venda cursor for
	select data_venda, cod_loja, cod_produto, cod_tipo_pagamento, cod_venda, volume, valor
	from tb_aux_venda
	where data_carga = @data_carga

	open c_venda
	fetch c_venda into @data_venda,
					   @cod_loja,
					   @cod_produto,
					   @cod_tipo_pagamento,
					   @cod_venda,
					   @volume,
					   @valor

	while (@@fetch_status = 0)
	begin
		set @id_tempo = (select id_tempo from dim_tempo where data = @data_venda)
		set @id_loja = (select id_loja from dim_loja where cod_loja = @cod_loja)
		set @id_produto = (select id_produto from dim_produto where cod_produto = @cod_produto)
		set @id_tipo_pagamento = (select id_tipo_pagamento from dim_tipo_pagamento where cod_tipo_pagamento = @cod_tipo_pagamento)

		if exists (select 1 from fato_venda
				   where cod_venda = @cod_venda)
		begin 
			update fato_venda
			set id_tempo = @id_tempo,
				id_loja = @id_loja,
				id_produto = @id_produto,
				id_tipo_pagamento = @id_tipo_pagamento,
				cod_venda = @cod_venda,
				volume = @volume,
				valor = @valor
			where cod_venda = @cod_venda
			end
			else
				insert into fato_venda(id_tempo,
									   id_loja,
									   id_produto,
									   id_tipo_pagamento,
									   cod_venda,
									   volume,
									   valor)
				values(@id_tempo,
					   @id_loja,
					   @id_produto,
					   @id_tipo_pagamento,
					   @cod_venda,
					   @volume,
					   @valor)
			fetch c_venda into @data_venda,
							   @cod_loja,
							   @cod_produto,
							   @cod_tipo_pagamento,
							   @cod_venda,
							   @volume,
							   @valor
	end
	close c_venda
	deallocate c_venda
end



-- Teste

exec sp_fato_venda '20230321'

select * from fato_venda