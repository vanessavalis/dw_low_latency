create or alter procedure sp_oltp_loja(@data_carga datetime)
as
begin
	delete from tb_aux_loja
	where data_carga = @data_carga
	insert into tb_aux_loja (data_carga,
							 cod_loja,
							 loja,
							 cidade,
							 estado,
							 sigla_estado)
	select @data_carga,
		   l.cod_loja,
		   l.nm_loja,
		   c.cidade,
		   e.estado,
		   e.sigla
	from tb_loja l 
		 inner join tb_cidade c
		 on (l.cod_cidade = c.cod_cidade)
		 inner join tb_estado e
		 on (c.cod_estado = e.cod_estado)
end



-- Teste

exec sp_oltp_loja '20230321'

select * from tb_aux_loja
