create or alter procedure sp_oltp_venda(@data_carga datetime, @data_inicial datetime, @data_final datetime)
as
begin
	delete from tb_aux_venda
	where data_carga = @data_carga
	insert into tb_aux_venda(data_carga,
							 data_venda,
							 cod_loja,
							 cod_produto,
							 cod_tipo_pagamento,
							 cod_venda,
							 volume,
							 valor)
	select @data_carga,
		   data_venda,
		   cod_loja,
		   cod_produto,
		   cod_tipo_pagamento,
		   cod_venda,
		   volume,
		   valor 
	from tb_venda
	where data_venda >= @data_inicial and
	      data_venda <= @data_final
end



-- Teste

exec sp_oltp_venda '20230321', '20230101', '20230701'

select * from tb_aux_venda