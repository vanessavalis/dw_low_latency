create or alter procedure sp_oltp_tipo_pagamento_i1(@data_carga datetime)
as
begin
	delete from tb_aux_tipo_pagamento
	where data_carga = @data_carga
	insert into tb_aux_tipo_pagamento(data_carga,
									  cod_tipo_pagamento,
									  tipo_pagamento)
	select @data_carga,
		   cod_tipo_pagamento,
		   tipo_pagamento
	from tb_tipo_pagamento
end



-- Teste

exec sp_oltp_tipo_pagamento_i1 '20230401'

select * from tb_aux_tipo_pagamento