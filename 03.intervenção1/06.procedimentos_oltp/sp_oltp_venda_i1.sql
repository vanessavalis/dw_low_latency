create or alter procedure sp_oltp_venda_i1(@data_carga datetime, @data_inicial datetime, @data_final datetime)
as
begin
    if (@data_inicial = @data_final) 
	   set @data_final = @data_inicial + 1
	delete from tb_aux_venda
	where data_carga = @data_carga
	insert into tb_aux_venda(data_carga,
							 data_venda,
							 cod_loja,
							 cod_produto,
							 cod_tipo_pagamento,
							 cod_venda,
							 volume,
							 valor)
	select @data_carga,
		   data_venda,
		   cod_loja,
		   cod_produto,
		   cod_tipo_pagamento,
		   cod_venda,
		   volume,
		   valor 
	from tb_venda
	where data_ultima_alteracao >= @data_inicial and
	      data_ultima_alteracao <= @data_final
end



-- Teste

exec sp_oltp_venda_i1 '20230401', '20230402', '20230402'

select * from tb_aux_venda